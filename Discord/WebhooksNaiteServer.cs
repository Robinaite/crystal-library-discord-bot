﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;

namespace CrystalLibraryBot.Discord
{
    public class WebhooksNaiteServer
    {
        DiscordWebhookClient dWebhookClient = new DiscordWebhookClient();
        Uri webhookUrl = new Uri("https://discord.com/api/webhooks/864263643557527612/ABWo-JrxGQUpYFM20AfHzooutXtIENt7qJ5xURZ0XRTbo7EzxrNH9acJtEs8LbIvS8w2");
        private Dictionary<DiscordWebhook, DiscordMessage> reloadingMessages = new Dictionary<DiscordWebhook, DiscordMessage>();

        public WebhooksNaiteServer()
        {
            dWebhookClient.AddWebhookAsync(webhookUrl);
        }
        
        public async Task SendReloadingMessage(string message)
        {
            DiscordWebhookBuilder webhookBuilder = new DiscordWebhookBuilder();
            webhookBuilder.WithContent("Reloading: " + message);
            reloadingMessages.Clear();
            reloadingMessages = await dWebhookClient.BroadcastMessageAsync(webhookBuilder);
        }

        public async Task DeleteReloadingMessage()
        {
            foreach (var reloadingMessage in reloadingMessages)
            {
                await reloadingMessage.Key.DeleteMessageAsync(reloadingMessage.Value.Id);
            }
        }
        
        public async Task SendMessageToMainServer(string message)
        {
            DiscordWebhookBuilder webhookBuilder = new DiscordWebhookBuilder();
            webhookBuilder.WithContent(message);
            await dWebhookClient.BroadcastMessageAsync(webhookBuilder);
        }
    }
}