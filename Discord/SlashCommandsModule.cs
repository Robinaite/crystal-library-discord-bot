﻿using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;

namespace CrystalLibraryBot.Discord
{
    public class SlashCommandsModule : SlashCommandModule
    {
        [SlashCommand("faq", "Obtains the Small Answer information from the Crystal Library website!")]
        public async Task FaqCommand(InteractionContext ctx,[Option("Tag", "The Tag of the page")]string tagName)
        {
            await MessageCreator.GetPageFromTag(tagName, ctx);
        }
        
        [SlashCommand("tags", "Search for all the similar tags to the one indicated")]
        public async Task TagsCommand(InteractionContext ctx,[Option("Tag", "The Tag to find")]string tagName)
        {
            var messageBuilder = MessageCreator.GetSimilarTags(tagName);
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DiscordInteractionResponseBuilder(messageBuilder));
        }
    }
}