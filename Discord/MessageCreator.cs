﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CrystalLibraryBot.Core;
using CrystalLibraryBot.Database;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;

namespace CrystalLibraryBot.Discord
{
    public static class MessageCreator
    {
        public static async Task GetPageFromTag(string tag, InteractionContext ctx)
        {
            DiscordFollowupMessageBuilder messageBuilder = new DiscordFollowupMessageBuilder();
            try
            {
                await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource,new DiscordInteractionResponseBuilder());
                PageFactory.Result result = PageFactory.GetPageFromMainTag(tag,out Page page);
                DiscordEmbedBuilder embedBuilder = new DiscordEmbedBuilder();
                switch (result)
                {
                    case PageFactory.Result.Ok:
                        await PageEmbed(page,ctx);
                        return;
                    case PageFactory.Result.NoTagFound:
                        embedBuilder = ListTagsEmbed(tag);
                        break;
                    case PageFactory.Result.TagEmpty:
                        embedBuilder = ErrorEmbed("Page with tag: " + tag + " has issues, please report to bot owner.");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                
                messageBuilder.AddEmbed(embedBuilder.Build());
                await ctx.FollowUpAsync(messageBuilder);
            }
            catch (Exception e)
            {
                WebhooksNaiteServer naiteServer = new WebhooksNaiteServer();
                await naiteServer.SendMessageToMainServer("Uh-oh, something went wrong: " + e.ToString());

                DiscordEmbedBuilder embedBuilder =
                    ErrorEmbed("Something *Huge* went wrong, contact Robin Naite (bot owner) about this issue.\n```" +
                               e.ToString() + "```");
                messageBuilder.AddEmbed(embedBuilder.Build());
                await ctx.FollowUpAsync(messageBuilder);
            }
        }
        
        public static DiscordMessageBuilder GetSimilarTags(string tag)
        {
            DiscordMessageBuilder messageBuilder = new DiscordMessageBuilder();
            try
            {
                DiscordEmbedBuilder embedBuilder = ListTagsEmbed(tag);
                messageBuilder.WithEmbed(embedBuilder.Build());
            }
            catch (Exception e)
            {
                DiscordEmbedBuilder embedBuilder =
                    ErrorEmbed("Something *Huge* went wrong, contact Robin Naite (bot owner) about this issue.\n```" +
                               e.ToString() + "```");
                messageBuilder.WithEmbed(embedBuilder.Build());
            }
            
            return messageBuilder;
        }
        
        
        private static async Task PageEmbed(Page page,InteractionContext ctx)
        {
            DiscordFollowupMessageBuilder followupMessageBuilder = new DiscordFollowupMessageBuilder();
            DiscordMessageBuilder messageBuilder = new DiscordMessageBuilder();
            
            var buttonPrev = new DiscordButtonComponent(ButtonStyle.Secondary,"previous","<",true);
            var buttonNext = new DiscordButtonComponent(ButtonStyle.Secondary,"next",">");


            var embedBuilder = SubPageEmbed(page.GetSubPage(0),page);
            followupMessageBuilder.AddEmbed(embedBuilder);
            if (!page.HasSubPage(1))
            {
                await ctx.FollowUpAsync(followupMessageBuilder);
                return;
            }
            followupMessageBuilder.AddComponents(buttonPrev,buttonNext);
            messageBuilder.AddComponents(buttonPrev, buttonNext);
            
            var message = await ctx.FollowUpAsync(followupMessageBuilder);
            var buttonResult = await message.WaitForButtonAsync(ctx.Member,(CancellationToken?)null);
            int currentSubPage = 0;
            while (!buttonResult.TimedOut)
            {
                if (buttonResult.Result.Id == "next")
                {
                    currentSubPage++;
                    if (!page.HasSubPage(currentSubPage + 1))
                    {
                        buttonNext.Disable();
                    }

                    buttonPrev.Enable();
                }
                else
                {
                    //Update to previous page
                    currentSubPage--;
                    if (!page.HasSubPage(currentSubPage - 1))
                    {
                        buttonPrev.Disable();
                    }

                    buttonNext.Enable();
                }
                
                SubPage newSubPage = page.GetSubPage(currentSubPage);
                if (newSubPage != null)
                {
                    embedBuilder = SubPageEmbed(newSubPage,page);
                    messageBuilder.WithEmbed(embedBuilder);
                    await buttonResult.Result.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage,new DiscordInteractionResponseBuilder(messageBuilder));
                }
                buttonResult = await message.WaitForButtonAsync(ctx.Member,(CancellationToken?)null);
            }
            messageBuilder.ClearComponents();
            messageBuilder.WithEmbed(embedBuilder);
 
            
            await message.ModifyAsync(messageBuilder);
        }

        private static DiscordEmbedBuilder SubPageEmbed(SubPage subPage, Page page)
        {
            DiscordEmbedBuilder embedBuilder = new DiscordEmbedBuilder();
            string subPageTitle = subPage.ObtainTitle();
            string relativeUrl = page.FilePath.Replace("\\", "/");
            if (!string.IsNullOrEmpty(subPageTitle))
            {
                string subPageUrl = subPageTitle.Replace(" ", "-").ToLower();
                relativeUrl += "#" + subPageUrl;
                subPageTitle += " - " + page.Title;
            }
            else
            {
                subPageTitle = page.Title;
            }
            embedBuilder.WithTitle(subPageTitle);
            embedBuilder = SetFields(subPage.ObtainContent(),embedBuilder,subPage.ObtainTitle());
            embedBuilder.WithFooter(page.ObtainAuthors());
            embedBuilder.WithUrl("https://crystal-library.robinaite.com" + relativeUrl);
            embedBuilder.WithColor(DiscordColor.Cyan);
            var imageUrl = subPage.ObtainImageUrl();
            if (!string.IsNullOrEmpty(imageUrl))
            {
                //Console.WriteLine(imageUrl.Replace("\\","/"));
                embedBuilder.WithImageUrl(imageUrl.Replace("\\","/"));
            }
            return embedBuilder;
        }

        private static DiscordEmbedBuilder SetFields(string content,DiscordEmbedBuilder embedBuilder, string subPageTitle)
        {

            Regex fieldsSplitRegex = new Regex("(?=^###[^#])", RegexOptions.Multiline);
            List<string> fields = new List<string>(fieldsSplitRegex.Split(content));

            if (fields.Count == 1)
            {
                var contentToEmbed = content;
                if (content.Length > 4096)
                {
                    contentToEmbed = content.Substring(0, 4000);
                    contentToEmbed += "\n\n**Too much text to show. Click on the title for the full page.**";
                }
                embedBuilder.WithDescription(contentToEmbed);
                return embedBuilder;
            }
            Regex fieldTitleRegex = new Regex("###[^#]\\s?(.*)");
            foreach (var field in fields)
            {
                if (string.IsNullOrEmpty(field))
                {
                    continue;
                }
                
                string fieldTitle = subPageTitle;
                var titleMatch = fieldTitleRegex.Match(field);
                if (titleMatch.Success)
                {
                    fieldTitle = titleMatch.Groups[1].Value.Trim();
                }
                string fieldContent = fieldTitleRegex.Replace(field, "").Trim();
                if (fieldContent.Length > 1024)
                {
                    fieldContent = fieldContent.Substring(0, 1024);
                }
                embedBuilder.AddField(fieldTitle, fieldContent);
            }

            return embedBuilder;
        }

        private static DiscordEmbedBuilder ErrorEmbed(string message)
        {
            DiscordEmbedBuilder embedBuilder = new DiscordEmbedBuilder();
            embedBuilder.WithTitle("Error!");
            embedBuilder.WithDescription(message);
            embedBuilder.WithColor(DiscordColor.Red);
            return embedBuilder;
        }
        
        private static DiscordEmbedBuilder ListTagsEmbed(string tag)
        {
            var similarTags = DatabaseConnection.ObtainTagsSimilarToTag(tag);

            if (similarTags.Count == 0)
            {
                return ErrorEmbed("No page with tag " + tag + " has been found.");
            }
            
            DiscordEmbedBuilder embedBuilder = new DiscordEmbedBuilder();
             embedBuilder.WithTitle("Are you looking for one of these?");
             string tagsDesc = "";

             foreach (var similarTag in similarTags) //TODO probably add a limiter in size, just in case
             {
                 tagsDesc += similarTag + "\n";
             }
            embedBuilder.WithDescription(tagsDesc);
            return embedBuilder;
        }
    }
}