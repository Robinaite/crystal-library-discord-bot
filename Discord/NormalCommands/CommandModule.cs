﻿using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace CrystalLibraryBot.Discord.NormalCommands
{
    public class CommandModule : BaseCommandModule
    {
        [Command("faq")]
        public async Task FaqCommand(CommandContext ctx,[RemainingText]string tag)
        {
            //await MessageCreator.GetPageFromTag(tag, ctx);
        }
        
        [Command("tags")]
        public async Task TagsCommand(CommandContext ctx,[RemainingText]string tag)
        {
            var messageBuilder = MessageCreator.GetSimilarTags(tag);
            
            await ctx.RespondAsync(messageBuilder);
        }
    }
}