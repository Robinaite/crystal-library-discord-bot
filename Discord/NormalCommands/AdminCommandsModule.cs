﻿using System;
using System.Threading.Tasks;
using System.Timers;
using CrystalLibraryBot.Core;
using CrystalLibraryBot.Database;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using LibGit2Sharp;

namespace CrystalLibraryBot.Discord.NormalCommands
{
    public class AdminCommandModule : BaseCommandModule
    {
        public DocsManager Docs { private get; set; }
        public WebhooksNaiteServer WebhooksNaiteServer { private get; set; }

        private Timer pullTimer;

        public AdminCommandModule()
        {
            if (pullTimer != null)
            {
                pullTimer.Stop();
                pullTimer.Dispose();
            }
            
            pullTimer = new Timer(21600 * 1000);
            pullTimer.Elapsed += (sender,e) => ReloadDocsTimerLapsed(sender,e,Docs);
            pullTimer.AutoReset = true;
            pullTimer.Start();
        }
        
        [Command("Reload"),RequireOwner]
        public async Task ReloadDatabaseCommand(CommandContext ctx)
        {
            var discordMsg = await ctx.RespondAsync("Start Updating Database");
            await Docs.UpdateDatabase();
            await discordMsg.DeleteAsync();
            await ctx.Message.DeleteAsync();
        }
        
        [Command("ReloadSetTimer"),RequireOwner]
        public async Task ReloadDatabaseTimerCommand(CommandContext ctx,int timeInSeconds)
        {
            var discordMsg = await ctx.RespondAsync("Setup Reload Database Timer");

            if (pullTimer != null)
            {
                pullTimer.Stop();
                pullTimer.Dispose();
            }
            
            pullTimer = new Timer(timeInSeconds * 1000);
            pullTimer.Elapsed += (sender,e) => ReloadDocsTimerLapsed(sender,e,Docs);
            pullTimer.AutoReset = true;
            pullTimer.Start();
            
            await Docs.UpdateDatabase();
            await discordMsg.DeleteAsync();
            await ctx.Message.DeleteAsync();
        }
        
        [Command("CloneDocs"),RequireOwner]
        public async Task CloneOfDocsCommand(CommandContext ctx)
        {
            try
            {
                await ctx.RespondAsync("Started Cloning");
                Repository.Clone("https://gitlab.com/Robinaite/crystal-library.git", "docs");
                await Docs.UpdateDatabase();
                await ctx.RespondAsync("Cloned and Updated!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await ctx.RespondAsync(e.ToString());
            }
        }
        
        
        [Command("PullDocs"),RequireOwner]
        public async Task PullDocsCommand(CommandContext ctx)
        {
            try
            {
                await ctx.RespondAsync("Started Pulling and updating");
                var repo = new Repository("docs");
                // Credential information to fetch
                var result = PullRepository(repo);
                Console.WriteLine(result.Status);
                if (result.Status != MergeStatus.Conflicts)
                {
                    await Docs.UpdateDatabase();
                }
                else
                {
                    await WebhooksNaiteServer.SendMessageToMainServer("Unable to pull the repository. - " + result.Status);
                }

                await ctx.RespondAsync("Pulled and Updated!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await ctx.RespondAsync(e.ToString());
            }
            
        }

        private static async void ReloadDocsTimerLapsed(Object source, ElapsedEventArgs e,DocsManager docs)
        {
            var repo = new Repository("docs");
            if (repo != null)
            {
                PullRepository(repo);
                await docs.UpdateDatabase();
            }
            else
            {
                Console.WriteLine("this is somehow Null");
            }
        }
        
        private static MergeResult PullRepository(Repository repo)
        {
            PullOptions options = new PullOptions();

            // User information to create a merge commit
            var signature = new Signature(
                new Identity("CL Bot", "MERGE_USER_EMAIL"), DateTimeOffset.Now);

            // Pull
            var result = Commands.Pull(repo, signature, options);
            Console.WriteLine(result.Status);
            return result;
        }
        [Command("DropDB"),RequireOwner]
        public async Task DropDatabaseCommand(CommandContext ctx)
        {
            await ctx.RespondAsync("Dropping Tables and ReCreating them. Use !clreload to add content.");
            DatabaseConnection.DropAllTables();
            DatabaseConnection.CreateTables();
        }
        
    }
}