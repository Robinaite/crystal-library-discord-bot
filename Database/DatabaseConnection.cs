﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using CrystalLibraryBot.Core;

namespace CrystalLibraryBot.Database
{
    public static class DatabaseConnection
    {
        
        private static SQLiteConnection sqlConnection;

        public static void CreateConnection()
        {
            // Create a new database connection:
            sqlConnection = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True;");
            // Open the connection:
            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
      
        
        //Make sure foreign_keys is on by doing PRAGMA foreign_keys; and PRAGMA foreign_keys = ON; to set if not
        public static void CreateTables() 
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return;
            }
            
            string createTables =
                $"CREATE TABLE IF NOT EXISTS Page(filePath TEXT PRIMARY KEY,title TEXT NOT NULL); " +
                $"CREATE TABLE IF NOT EXISTS Tag(tag VARCHAR(20) PRIMARY KEY COLLATE NOCASE); " +
                $"CREATE TABLE IF NOT EXISTS TagPage(tag VARCHAR(20) COLLATE NOCASE, filePath TEXT,isMainTag INT NOT NULL, PRIMARY KEY(tag,filePath),FOREIGN KEY (tag) REFERENCES Tag (tag),FOREIGN KEY (filePath) REFERENCES Page (filePath)); " +
                $"CREATE TABLE IF NOT EXISTS SubPage(subPageID INT,filePath TEXT,title TEXT,subPage TEXT,PRIMARY KEY(subPageID,filePath),FOREIGN KEY (filePath) REFERENCES Page (filePath));" +
                $"CREATE TABLE IF NOT EXISTS SubPageImages(subPageID INT,filePath TEXT,imageUrl TEXT ,PRIMARY KEY (subPageID,filePath,imageUrl),FOREIGN KEY (subPageID) REFERENCES SubPage (subPageID),FOREIGN KEY (filePath) REFERENCES Page (filePath)); " +
                $"CREATE TABLE IF NOT EXISTS SubPageToPage(subPageID INT, filePath TEXT,FOREIGN KEY (subPageID) REFERENCES SubPage (subPageID),FOREIGN KEY (filePath) REFERENCES Page (filePath));" +
                $"CREATE TABLE IF NOT EXISTS Author(author VARCHAR(30) PRIMARY KEY COLLATE NOCASE);" +
                $"CREATE TABLE IF NOT EXISTS AuthorPage(author VARCHAR(20) COLLATE NOCASE, filePath TEXT,PRIMARY KEY (author,filePath),FOREIGN KEY (author) REFERENCES Author (author),FOREIGN KEY (filePath) REFERENCES Page (filePath));";
                
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = createTables;
            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                //Table exists do nothing
                Console.WriteLine(ex.Message);
            }
        }

        public static void DropAllTables()
        {
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = $"drop table AuthorPage; " +
                                    $"drop table Author; " +
                                    $"drop table SubPageImages; " +
                                    $"drop table SubPageToPage;" +
                                    $"drop table SubPage;" +
                                    $"drop table TagPage;" +
                                    $"drop table Page;" +
                                    $"drop table Tag;";
            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e);
            }
        }
        
        
        public static void InsertTag(string tag)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return;
            }
            
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "INSERT OR REPLACE INTO Tag(tag) VALUES (?);";
            sqliteCmd.Parameters.AddWithValue("tag", tag);

            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(tag);
                Console.WriteLine(e);
            }
        }

        public static void InsertAuthor(string author)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return;
            }
            
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "INSERT OR REPLACE INTO Author(author) VALUES (?);";
            sqliteCmd.Parameters.AddWithValue("author", author);

            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(author);
                Console.WriteLine(e);
            }
        }

        public static void InsertTagToPage(string tag, string filePath, bool mainTag = false)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return;
            }
            
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "INSERT OR REPLACE INTO TagPage(tag,filePath,isMainTag) VALUES (?,?,?);";
            sqliteCmd.Parameters.AddWithValue("tag", tag);
            sqliteCmd.Parameters.AddWithValue("filePath", filePath);
            sqliteCmd.Parameters.AddWithValue("isMainTag", mainTag ? 1 : 0);

            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e);
            }
        }

        public static void InsertAuthorToPage(string author, string filePath)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return;
            }
            
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "INSERT OR REPLACE INTO AuthorPage(author,filePath) VALUES (?,?);";
            sqliteCmd.Parameters.AddWithValue("author", author);
            sqliteCmd.Parameters.AddWithValue("filePath", filePath);

            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e);
            }
        }

        public static void InsertPage(string filePath, string title)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return;
            }
            
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "INSERT OR REPLACE INTO Page(filePath,title) VALUES (?,?);";
            sqliteCmd.Parameters.AddWithValue("filePath", filePath);
            sqliteCmd.Parameters.AddWithValue("title", title);

            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e);
            }
        }
        
        public static void InsertSubPage(string filePath, SubPage subPage)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return;
            }
            
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "INSERT OR REPLACE INTO SubPage(subPageID,filePath,title,subPage) VALUES (?,?,?,?);";
            sqliteCmd.Parameters.AddWithValue("subPageID", subPage.ObtainId());
            sqliteCmd.Parameters.AddWithValue("filePath", filePath);
            sqliteCmd.Parameters.AddWithValue("title", subPage.ObtainTitle());
            sqliteCmd.Parameters.AddWithValue("subPage", subPage.ObtainContent());
  
            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e);
            }
        }
        public static void InsertSubPageImages(string filePath,SubPage subPage)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return;
            }

            string url = subPage.ObtainImageUrl();
            if (string.IsNullOrEmpty(url))
            {
                return;
            }
            SQLiteCommand sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "INSERT OR REPLACE INTO SubPageImages(subPageID,filePath,imageUrl) VALUES (?,?,?);";
            sqliteCmd.Parameters.AddWithValue("subPageID", subPage.ObtainId());
            sqliteCmd.Parameters.AddWithValue("filePath", filePath);
            sqliteCmd.Parameters.AddWithValue("imageUrl", url);
  
            try
            {
                sqliteCmd.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e);
            }
        }
        

        public static string ObtainFilePathFromMainTag(string tag)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return "";
            }
            
            var sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "SELECT filePath FROM TagPage WHERE tag = @tag AND isMainTag = 1";
            sqliteCmd.Parameters.AddWithValue("@tag", tag);
            using var sqliteReader = sqliteCmd.ExecuteReader();
            if (sqliteReader.Read())
            {
                return sqliteReader.GetString(0);
            }

            return "";
        }
        
        public static string ObtainFilePathFromFirstTag(string tag)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return "";
            }
            
            var sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "SELECT filePath FROM TagPage WHERE tag = @tag";
            sqliteCmd.Parameters.AddWithValue("@tag", tag);
            using var sqliteReader = sqliteCmd.ExecuteReader();
            if (sqliteReader.Read())
            {
                return sqliteReader.GetString(0);
            }

            return "";
        }
        
        public static string ObtainTitleFromFilePath(string filePath)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                return null;
            }
            
            var sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "SELECT title FROM Page WHERE filePath = @filePath;";
            sqliteCmd.Parameters.AddWithValue("@filePath",filePath);
   
            using (var sqliteReader = sqliteCmd.ExecuteReader())
            {
                if (sqliteReader.Read())
                {
                    return sqliteReader.GetString(0);
                }
            }

            return null;
        }

        public static List<SubPage> ObtainAllSubpages(string filePath)
        {
            
            if (sqlConnection.State != ConnectionState.Open)
            {
                return null;
            }
   
            var sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "SELECT * FROM SubPage WHERE filePath = @filePath;";
            sqliteCmd.Parameters.AddWithValue("@filePath",filePath);
            List<SubPage> subPages = new List<SubPage>();
            using (var sqliteReader = sqliteCmd.ExecuteReader())
            {
                
                while (sqliteReader.Read())
                {
                    subPages.Add(new SubPage(sqliteReader.GetInt32(0),sqliteReader.GetString(2),sqliteReader.GetString(3), isCleaned: true,filePath));
                }
                    
            }

            foreach (var subPage in subPages)
            {
                var subPageImages = ObtainSubPageImages(filePath, subPage.ObtainId());
                subPage.SetImageList(subPageImages);
            }

            return subPages;
        }
        
        public static List<string> ObtainTagsSimilarToTag(string tag)
        {
            List<string> similarTags = new List<string>();
            if (sqlConnection.State != ConnectionState.Open)
            {
                return similarTags;
            }
            
            var sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "SELECT * FROM tag WHERE tag LIKE @tag";
            sqliteCmd.Parameters.AddWithValue("@tag", "%"+tag+"%");
            using var sqliteReader = sqliteCmd.ExecuteReader();
            while (sqliteReader.Read())
            {
                similarTags.Add(sqliteReader.GetString(0));
            }

            return similarTags;
        }
        
        public static List<string> ObtainSubPageImages(string filePath, int subPageID)
        {
            List<string> subPageImages = new List<string>();
            if (sqlConnection.State != ConnectionState.Open)
            {
                return subPageImages;
            }
            
            var sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "SELECT imageUrl FROM SubPageImages WHERE SubPageImages.filePath = @filePath AND subPageID = @subPageId";
            sqliteCmd.Parameters.AddWithValue("@filePath",filePath );
            sqliteCmd.Parameters.AddWithValue("@subPageId",subPageID );
            using var sqliteReader = sqliteCmd.ExecuteReader();
            while (sqliteReader.Read())
            {
                subPageImages.Add(sqliteReader.GetString(0));
            }

            return subPageImages;
        }
        
        public static List<string> ObtainAuthors(string filePath)
        {
            List<string> authors = new List<string>();
            if (sqlConnection.State != ConnectionState.Open)
            {
                return authors;
            }
            
            var sqliteCmd = sqlConnection.CreateCommand();
            sqliteCmd.CommandText = "SELECT author FROM AuthorPage WHERE filepath LIKE @filePath";
            sqliteCmd.Parameters.AddWithValue("@filePath", filePath);
            using var sqliteReader = sqliteCmd.ExecuteReader();
            while (sqliteReader.Read())
            {
                authors.Add(sqliteReader.GetString(0));
            }

            return authors;
        }
       
    }
}