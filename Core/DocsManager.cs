﻿using System.IO;
using System.Threading.Tasks;
using CrystalLibraryBot.Database;
using CrystalLibraryBot.Discord;

namespace CrystalLibraryBot.Core
{
    public class DocsManager
    {
        private static bool isReloadGoingOn;
        // SQLLite Database
        private WebhooksNaiteServer naiteServerWebhook = new WebhooksNaiteServer();

        public async Task UpdateDatabase()
        {
            if (isReloadGoingOn)
            {
                return;
            }

            isReloadGoingOn = true;
            try
            {
                await naiteServerWebhook.SendReloadingMessage("Database is Reloading");
                DatabaseConnection.DropAllTables();
                DatabaseConnection.CreateTables();//TODO I dont like this here, figure out something better
                foreach (string filePath in Directory.EnumerateFiles(Directory.GetCurrentDirectory() + "/docs", "*.md",
                    SearchOption.AllDirectories))
                {
                    UpdatePageInformationAsync(filePath);
                }
            }
            catch (DirectoryNotFoundException)
            {

            }
            finally
            {
                isReloadGoingOn = false;
            }
        }

        /// <summary>
        /// Obtains the page information if it contains a Tag
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>True if it contains a Tag, false if not</returns>
        private void UpdatePageInformationAsync(string filePath)
        {
            string filePathEdited = filePath.Replace(Directory.GetCurrentDirectory() + "/docs", "");
            filePathEdited = filePathEdited.Replace(".md", "");
            StreamReader file = new StreamReader(filePath);

            Page newPage = PageFactory.CreatePageFromMarkdown(filePathEdited, file.ReadToEnd());
            if (newPage == null)
            {
                return;
            }
            
            newPage.InsertIntoDatabase();
        }
    }
}