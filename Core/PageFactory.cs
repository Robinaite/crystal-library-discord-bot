﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CrystalLibraryBot.Database;

namespace CrystalLibraryBot.Core
{
    public static class PageFactory
    {

        public enum Result
        {
            Ok,
            NoTagFound,
            TagEmpty,
            MultipleTagsFound,
        }
        
        public static Result GetPageFromMainTag(string tag,out Page newPage)
        {
            newPage = new Page();
            if (string.IsNullOrEmpty(tag))
            {
                return Result.TagEmpty;
            }

            string filePath = DatabaseConnection.ObtainFilePathFromMainTag(tag);

            if (string.IsNullOrEmpty(filePath))
            {
                filePath = DatabaseConnection.ObtainFilePathFromFirstTag(tag);
                if (string.IsNullOrEmpty(filePath))
                {
                    return Result.NoTagFound;
                }
            }

            newPage.SetFilePath(filePath);
            
            var subPages = DatabaseConnection.ObtainAllSubpages(filePath);
            if (subPages == null)
            {
                throw new Exception("Tag page exists, but has no subPages, report to Bot Owner for fix. Tag: " + tag);
            }
            else
            {
                newPage.AddSubPage(subPages);
            }
            

            string title = DatabaseConnection.ObtainTitleFromFilePath(filePath);
            if (string.IsNullOrEmpty(title))
            {
                throw new Exception("Tag page exists, but has no title, report to Bot Owner for fix. Tag:" + tag);
            }
            else
            {
                newPage.SetTitle(title);
            }
            
            List<string> authors = DatabaseConnection.ObtainAuthors(filePath);
            foreach (var author in authors)
            {
                newPage.AddAuthor(author);
            }

            return Result.Ok;
        }
        
        public static Page CreatePageFromMarkdown(string filePath, string nonEditedPage)
        {
            Page newPage = new Page();
            newPage.SetFilePath(filePath);
            
            Regex metaDataRegex = new Regex("^---.*---",RegexOptions.Singleline);
            var metaDataMatch = metaDataRegex.Match(nonEditedPage);
            bool hasTags = SetMetaData(metaDataMatch,newPage);
            if (!hasTags)
            {
                //This page has no Tags so should be ignored. 
                return null;
            }
            
            Regex contentRegex = new Regex("#(.*\n)*");
            var contentMatch = contentRegex.Match(nonEditedPage);
            bool hasContent = SetContent(contentMatch,newPage);
            if (!hasContent)
            {
                //this page did not get content, probably bugged
                return null;
            }
            
            return newPage;
        }

        private static bool SetMetaData(Match metaDataMatch,Page newPage)
        {
            if (!metaDataMatch.Success)
            {
                //Page has no meta data, cancel operation
                return false;
            }
            
            Regex tagsRegex = new Regex("tags:\\s*(\r\n|\r|\n)(\\s*-.+\n)+");
            var tagsMatch = tagsRegex.Match(metaDataMatch.Value);
            bool hasTags = SetTagsData(tagsMatch,newPage);
            if (!hasTags)
            {
                //Page has no Tags, cancel operation
                return false;
            }
            
            Regex authorsRegex = new Regex("authors:\\s*(\r\n|\r|\n)(\\s*-.+\n)+");
            var authorsMatch = authorsRegex.Match(metaDataMatch.Value);
            SetAuthorData(authorsMatch,newPage);

            return true;
        }

        private static bool SetTagsData(Match tagsMetaData, Page newPage)
        {
            if (!tagsMetaData.Success)
            {
                //The page does not have any tags meta data
                return false;
            }
            Regex tagsRegex = new Regex("(?:-\\s).*");
            var tagsMatches = tagsRegex.Matches(tagsMetaData.Value);
            
            foreach (Match tagsMatch in tagsMatches)
            {
                if (tagsMatch.Length > 0)
                {
                    string tag = tagsMatch.Value.Substring(1);
                    tag = tag.Trim();
                    newPage.AddTag(tag);
                }
            }

            return true;
        }

        private static void SetAuthorData(Match authorMetaData,Page newPage)
        {
            if (!authorMetaData.Success)
            {
                //The page does not have any Author meta data, it should have!!
                //Friendly warning but no stop adding of the page
            }
            Regex authorRegex = new Regex("(?:-\\s).*");
            var authorMatches = authorRegex.Matches(authorMetaData.Value);
            
            foreach (Match authorMatch in authorMatches)
            {
                if (authorMatch.Length > 0)
                {
                    string author = authorMatch.Value.Substring(1);
                    author = author.Trim();
                    newPage.AddAuthor(author);
                }
            }
        }

        private static bool SetContent(Match content,Page newPage)
        {
            if (!content.Success)
            {
                return false;
            }

            //Set title
            SetTitle(content,newPage);

            SetSubPages(content,newPage);

            return true;
        }

        private static void SetTitle(Match content, Page newPage)
        {
            Regex titleRegex = new Regex("^#[^#]\\s?(.*)");
            var titleMatch = titleRegex.Match(content.Value);

            if (titleMatch.Success)
            {
                newPage.SetTitle(titleMatch.Groups[1].Value);
            }
        }

        private static void SetSubPages(Match content,Page newPage)
        {
            Regex contentOnlyRegex = new Regex("^#[^#].*", RegexOptions.Multiline);
            var contentOnly = contentOnlyRegex.Replace(content.Value, "");
            contentOnly = contentOnly.Trim();

            Regex subPagesRegex = new Regex("(?=^##[^#])", RegexOptions.Multiline);
            var subPagesNonCleaned = new List<string>(subPagesRegex.Split(contentOnly));


            for (var index = 0; index < subPagesNonCleaned.Count; index++)
            {
                var subPageNonCleaned = subPagesNonCleaned[index];
                if (string.IsNullOrEmpty(subPageNonCleaned))
                {
                    //ignore this entry
                    continue;
                }
                
                Regex subPageTitleRegex = new Regex("^##[^#]\\s?(.*)",RegexOptions.Multiline);
                var titleMatch = subPageTitleRegex.Match(subPageNonCleaned);
                var subPageTitle = "";
                if (titleMatch.Success)
                {
                    subPageTitle = titleMatch.Groups[1].Value.Trim();
                    subPageNonCleaned = subPageTitleRegex.Replace(subPageNonCleaned, "").Trim();
                }

                SubPage newSubPage = new SubPage(index,subPageTitle,subPageNonCleaned, false,newPage.FilePath);
                newPage.AddSubPage(newSubPage);
            }
        }
        
    }
}