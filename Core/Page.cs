﻿using System.Collections.Generic;
using CrystalLibraryBot.Database;

namespace CrystalLibraryBot.Core
{
    public class Page
    {
        public string FilePath { get; private set; }

        /// <summary>
        /// First one in the Tags list is the Main Tag
        /// </summary>
        private List<string> tags = new List<string>();
        private List<string> authors = new List<string>();
        public string Title { get; private set; } = "";
        private List<SubPage> subPages = new List<SubPage>();//TODO this should not be public, change when we swap to full reading subPages

        public void SetFilePath(string pageFilePath)
        {
            FilePath = pageFilePath;
        }

        public void AddTag(string tag)
        {
            tags.Add(tag);
        }

        public void AddAuthor(string author)
        {
            authors.Add(author);
        }
        
        public void SetTitle(string pageTitle)
        {
            Title = pageTitle;
        }

        public void AddSubPage(SubPage subPage)
        {
            subPages.Add(subPage);
        }
        
        public void AddSubPage(IEnumerable<SubPage> subPagesList)
        {
            subPages = new List<SubPage>(subPagesList);
        }

        public string ObtainAuthors()
        {
            string authorsString = "Authors: ";
            foreach (var author in authors)
            {
                authorsString += author + ", ";
            }

            return authorsString;
        }

        public SubPage GetSubPage(int index)
        {
            if (subPages.Count <= index)
            {
                return null;
            }
            return subPages[index];
        }

        public bool HasSubPage(int index)
        {
            return index >= 0 && index < subPages.Count;
        }

        public void InsertIntoDatabase()
        {
            //Insert Page
            DatabaseConnection.InsertPage(FilePath,Title);

            foreach (var subPage in subPages)
            {
                DatabaseConnection.InsertSubPage(FilePath,subPage);
                DatabaseConnection.InsertSubPageImages(FilePath,subPage);
            }
            
            //InsertTags
            bool mainTagSet = false;
            foreach (var tag in tags)
            {
                DatabaseConnection.InsertTag(tag);
                DatabaseConnection.InsertTagToPage(tag,FilePath,!mainTagSet);
                mainTagSet = true;
            }
            
            //Insert Authors
            foreach (var author in authors)
            {
                DatabaseConnection.InsertAuthor(author);
                DatabaseConnection.InsertAuthorToPage(author,FilePath);
            }
        }
    }
}