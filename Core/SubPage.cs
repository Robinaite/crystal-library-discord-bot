﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CrystalLibraryBot.Core
{
    public class SubPage
    {
        private int subPageID;
        /// <summary>
        /// Careful, the subPage Title CAN be empty!
        /// </summary>
        private string subPageTitle = "";
        private string content = "";
        private List<string> imageUrls = new List<string>();

        public SubPage(int subPageId,string title,string contentPage,bool isCleaned,string filePath)
        {
            content = contentPage;
            this.subPageID = subPageId;
            subPageTitle = title;
            //obtain subpage title
            if (isCleaned)
            {
                return;
            }
            
            Regex ytMacroRegex = new Regex("{{\\s?yt\\(\"(.*)\"\\)\\s?}}");
            content = ytMacroRegex.Replace( content, "[https://www.youtube.com/watch?v=$1](https://www.youtube.com/watch?v=$1)");

            //remove blank from urls
            Regex urlMacroRegex = new Regex("{target=_blank}");
            content = urlMacroRegex.Replace(content, "");

            // Tooltip stuff
            Regex tooltipMacroRegex = new Regex("({{\\s?)|(\\s?}})");
            content = tooltipMacroRegex.Replace(content, "");
                
            //Relative Urls for interlinking markdowns
            Regex relativeUrlsRegex = new Regex("\\]\\((.*).md\\)");
            content = relativeUrlsRegex.Replace(content,"](https://crystal-library.robinaite.com/$1)");
                
            //images
            Regex imageUrlsRegex = new Regex("!\\[(.*)\\]\\((.*)\\)");
            var imageUrlsMatchCollection = imageUrlsRegex.Matches(content);

            Regex pathSplitRegex = new Regex("\\\\|/");
            var paths = pathSplitRegex.Split(filePath);
            string path = "";
            if (paths.Length > 1)
            {
                for(int i = 0; i< paths.Length-1;i++)
                {
                    path += paths[i] + "/";
                }
            }
            
            
            foreach (Match imageUrlMatch in imageUrlsMatchCollection)
            {
                if (imageUrlMatch.Groups.Count == 3)
                {
                    var imageUrl = "https://crystal-library.robinaite.com" + path + imageUrlMatch.Groups[2].Value.Replace("\\","/");
                    imageUrls.Add(imageUrl);
                }
            }
                
            content = imageUrlsRegex.Replace(content,"[$1 Image](https://crystal-library.robinaite.com"+ path+"$2)");
            
            //remove admonitions for discord (!!!|\?\?\?).*\n(\s{4}.*)*
            Regex admonitionsRegex = new Regex("(!!!|\\?\\?\\?).*\\n(\\s{4}.*)*");
            content = admonitionsRegex.Replace(content, "");
        }

        public string ObtainContent()
        {
            return content;
        }

        public int ObtainId()
        {
            return subPageID;
        }

        public string ObtainTitle()
        {
            return subPageTitle;
        }
        
        /// <summary>
        /// Returns up to the four first urls in the list.
        /// </summary>
        /// <returns>A list with up to 4 urls</returns>
        public string ObtainImageUrl()
        {
            // List<string> firstFourUrls = new List<string>(imageUrls);
            // if (firstFourUrls.Count > 4)
            // {
            //     firstFourUrls.RemoveRange(4,firstFourUrls.Count-4);
            // }

            if (imageUrls.Count > 0)
            {
                return imageUrls[0];
            }
            
            return "";
        }

        public void SetImageList(List<string> subImagesUrls)
        {
            imageUrls = new List<string>(subImagesUrls);
        }
        
    }
}