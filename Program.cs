﻿using System;
using System.Threading.Tasks;
using CrystalLibraryBot.Core;
using CrystalLibraryBot.Database;
using CrystalLibraryBot.Discord;
using CrystalLibraryBot.Discord.NormalCommands;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using Microsoft.Extensions.DependencyInjection;

namespace CrystalLibraryBot
{
    class Program
    {
        static void Main()
        {
            DatabaseConnection.CreateConnection();
            DatabaseConnection.CreateTables();
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            string token = Environment.GetEnvironmentVariable("CL_DISCORD_BOT_KEY");
            var discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = token,
                TokenType = TokenType.Bot
            });
            discord.UseInteractivity(new InteractivityConfiguration()
            {
                ResponseBehavior = InteractionResponseBehavior.Respond,
                ResponseMessage = "Getting the Next page!"
            });

            var serviceCollection = new ServiceCollection()
                .AddSingleton<DocsManager>()
                .BuildServiceProvider();

            var slash = discord.UseSlashCommands(new SlashCommandsConfiguration()
            {
                Services = serviceCollection,
            });

            slash.RegisterCommands<SlashCommandsModule>();

            var commands = discord.UseCommandsNext(new CommandsNextConfiguration()
            {
                Services = serviceCollection,
                StringPrefixes = new[] {"!cl"}
            });

            commands.RegisterCommands<AdminCommandModule>();
            //commands.RegisterCommands<CommandModule>();

            //await SendToNaiteDiscordWebhook("test lul");
            await discord.ConnectAsync();
            await Task.Delay(-1);
        }
    }
}